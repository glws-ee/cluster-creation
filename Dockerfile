# This Dockerfile uses an Ubuntu 20.04 LTS container with Tanzu tools & adds kubectl(latest), helm(latest 3.x), terraform (latest) & GitLab's Terraform extensions (latest) to it.

FROM jasonmorgan/gitlab-tools:latest

LABEL maintainer="khair@gitlab.com"
LABEL version="2020070801"
LABEL description="Tanzu tools + kubectl + helm + Terraform + GitLab Terraform tools"

WORKDIR /tmp

RUN apt-get update && apt-get upgrade -yq && \
    apt-get install -yq gettext curl wget python3 unzip && \
    curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl && \
    chmod u+x kubectl && \
    mv kubectl /usr/local/bin

RUN curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 && \
    chmod 700 get_helm.sh && \
    ./get_helm.sh

RUN export latest_version=`curl https://checkpoint-api.hashicorp.com/v1/check/terraform | python3 -mjson.tool | grep current_version | awk '{print $2}' | sed s/\"\//g | sed s/,//g` && \
    echo $latest_version && \
    wget "https://releases.hashicorp.com/terraform/${latest_version}/terraform_${latest_version}_linux_amd64.zip" && \
    unzip *.zip && \
    rm -rf *.zip && \
    mv terraform /usr/local/bin && \
    wget https://gitlab.com/gitlab-org/terraform-images/-/blob/master/src/bin/gitlab-terraform.sh && \
    mv gitlab-terraform.sh /usr/local/bin/gitlab-terraform && \
    chmod u+x /usr/local/bin/gitlab-terraform && \
    apt-get remove -yq unzip wget python3 curl && \
    apt-get autoremove -yq && \
    cd .. && \
    rm -rf *.zip
