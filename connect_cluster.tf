provider gitlab {
  token = var.gitlab_token
}

resource gitlab_group_cluster "K8s-cluster" {
  group                       = var.group
  name                        = var.cluster_name
  kubernetes_api_url          = var.kubernetes_api_url
  kubernetes_token            = var.kubernetes_token
  kubernetes_ca_cert          = var.kubernetes_ca_cert
  domain                      = var.domain
}

# When `terraform output` runs it will spit out the following variable.
# The cluster_name will be used to destroy the cluster created earlier in the
# integrated pipeline

output "cluster_name" {
  value = var.cluster_name
}

output "group" {
  value = var.group
}

output "domain" {
  value = var.domain
}
