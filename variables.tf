variable "gitlab_token" {
  description = "GitLab CI/CD variable - 'gitlab_token'. The Personal Access token with API access. THIS MUST BE CREATED OR YOUR DEMO WILL NOT WORK!."
}

variable "gitlab_url" {
  description = "GitLab CI/CD variable - 'gitlab_url'. This defaults to gitlab.com/SaaS. Otherwise, set here or export TF_VAR_gitlab_url if you are using self managed."
}

variable "kubernetes_token" {
  description = "Created by GitLab CI/CD & exported for Terraform"
}

variable "kubernetes_api_url" {
  description = "Created by GitLab CI/CD job & exported for Terraform"
}

variable "kubernetes_ca_cert" {
  description = "Created by GitLab CI/CD job & exported for Terraform"
}

variable "group" {
  description = "GitLab CI/CD variable - 'group'. Matches the group number for the EKS cluster to attach"
}

variable "cluster_name" {
  description = "GitLab CI/CD variable - 'cluster_name'. This is the name of the EKS cluster"
}

variable "domain" {
  default = "bottlerocket.gl-demo.io"
  description = "GitLab CI/CD variable - 'domain'. This is the domain name to use in the cluster. It is OPTIONAL and defaults to bottlerocket.gl-demo.io"
}
