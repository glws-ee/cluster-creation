# EKS Cluster Creation & Group Attachment

THIS PROJECT IS UNDER ACTIVE DEVELOPMENT. README DOES NOT CONTAIN ALL INFORMATION NEEDED

Amazon's Bottlerocket is an Open Source purpose built container operating system. The files in this project will be used to validate GitLab's support for the platform.

## What does this project do?

This project's sole purpose is to build a Bottlerocket based EKS cluster & store the cluster details in an S3 bucket for later retrieval.

The [GitLab CI/CD pipeline](.gitlab-ci.yml) does the following:
* Builds a helper container with tools needed to build/deploy the EKS cluster
* Builds the EKS cluster using Bottlerocket EC2 instances via `eksctl`
* Stores the EKS cluster credentials in a private S3 bucket
* Provides a mechanism to manually destroy the created EKS cluster


The helper container includes the following:
* GitLab's AWS Cloud deployment container as the [base image](https://docs.gitlab.com/ee/ci/cloud_deployment/#aws)
* Creation of EKS clusters using [`eksctl`](https://github.com/weaveworks/eksctl)
* Management of Kubernetes clusters using [`kubectl`](https://github.com/kubernetes/kubectl)

The credentials stored in the S3 bucket are used for a [sister project](https://gitlab.com/gitlab-com/alliances/aws/sandbox-projects/bottlerocket-support-auto-devops) that executes a pipeline to test out GitLab's build & testing containers commonly used by GitLab's customers.

## Notes & Limitations

The project currently uses hardcoded variables. Typically, these would be defined in the CI/CD variables and passed into the pipeline.

This project is unsupported; it's used mainly to show how to create & destroy a Bottlerocket based EKS cluster.
